<?php 

require_once 'Compte.php';

class Banque implements ArrayAccess
{
    public $nom;
    private $comptes;

    public function __construct($nom)
    {
        $this->nom = $nom;
        $this->comptes = [];
    }

    // $banque = new Banque("nom");
    // $banque["0001"] = $value;
    public function offsetSet($offset, $value) {
        
    }

    // $banque = new Banque("nom");
    // if($banque["0001"])
    public function offsetExists($offset) {
        return isset($this->comptes[$offset]);
    }

    // $banque = new Banque("nom");
    // unset($banque["0001"])
    public function offsetUnset($offset) {
        //unset($this->comptes[$offset]);
    }

    // $banque = new Banque("nom");
    // $banque["0001"]->depot();
    public function offsetGet($offset) {
        /*
        if(isset($this->comptes[$offset]))
        {
            return $this->comptes[$offset];
        }
        return null;

        Ternary operator
        */
        return isset($this->comptes[$offset]) ? $this->comptes[$offset] : null;
    }

    public function AjouterCompte(Compte $compte)
    {
        $this->comptes[$compte->getNumero()] = $compte;
    }

    public function SupprimerCompte($numero)
    {
        unset($this->comptes[$numero]);
    }
}

?>