<?php 

require_once 'Framework/Controller.php';
require_once 'Framework/View.php';
require_once 'Model/Personne.php';
require_once 'Model/Compte.php';

class ControllerCompte extends Controller
{
    public function index()
    {
        $this->list();
    }

    public function list()
    {
        $comptes = Compte::getAll();

        $view = new View("compte_list");
        $view->show(["comptes" => $comptes]);
    }
}

?>