<?php include('header.html') ?>
<?php include('menu_base.html') ?>

<div class="page-header">
    <h1>Persons : </h1>
</div>

<div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Nom</th>
                <th scope="col">Prenom</th>
                <th scope="col">Date de naissance</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($list as $person): ?>
                <tr>
                    <td scope="row"> <?= $person->id ?> </td>
                    <td scope="row"> <?= $person->nom ?> </td>
                    <td scope="row"> <?= $person->prenom ?> </td>
                    <td scope="row"> <?= $person->convertDate() ?> </td>
                    <td scope="row">
                        <div class="btn-group" role="group">
                            <a href="personne/edit/<?= $person->id ?>" class="btn btn-primary">edit</a>
                            <a href="personne/info/<?= $person->id ?>" class="btn btn-info">info</a>
                            <a href="personne/delete/<?= $person->id ?>" class="btn btn-danger">delete</a>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <hr/>
    <a href="personne/create" class="btn btn-primary">Create person</a>
</div>

<?php include('footer.html') ?>