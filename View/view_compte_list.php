<?php include('header.html') ?>
<?php include('menu_base.html') ?>

<div class="page-header">
    <h1>Comptes : </h1>
</div>

<div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Numero</th>
                <th scope="col">Titulaire</th>
                <th scope="col">Solde</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($comptes as $compte): ?>
                <tr>
                    <td scope="row"> <?= $compte->getNumero() ?></td>
                    <td scope="row"> <?= $compte->getTitulaire() ?></td>
                    <td scope="row"> <?= $compte->getSolde() ?></td>
                    <td scope="row">
                        <div class="btn-group" role="group">
                            <a href="compte/edit/<?= $compte->getNumero() ?>" class="btn btn-primary">Edit</a>
                            <a href="compte/info/<?= $compte->getNumero() ?>" class="btn btn-info">Info</a>
                            <a href="compte/delete/<?= $compte->getNumero() ?>" class="btn btn-danger">Delete</a>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <hr/>
    <a href="compte/create" class="btn btn-primary">Create account</a>
</div>

<?php include('footer.html') ?>