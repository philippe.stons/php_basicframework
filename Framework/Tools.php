<?php 

require_once 'View.php';

class Tools
{
    public static function sanitize($var)
    {
        $var = stripslashes($var);
        $var = htmlspecialchars($var);
        $var = strip_tags($var);

        return $var;
    }

    public static function abort($err)
    {
        $view = new View("error");
        $view->show(["error" => $err]);
        die;
    }
}

?>